﻿CREATE DATABASE 
  ejemplo2Yii2Desarrollo;

use ejemplo2Yii2Desarrollo;

create or replace table catalogo(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  descripcion varchar(100),
  primary key(id)
  );

INSERT INTO catalogo VALUES 
  (1,'flor1','Flor del campo'),
  (2,'flor2','Flor del bosque');

